#pragma once
#include <stdio.h>
class object
{
public:
	int _num;
	object()
	{
		_num = 0;
	}

	object(int n) {
		_num = n;
	}
	bool operator<(const object& other)
	{
		return _num < other._num;
	}
	bool operator>(const object& other)
	{
		return _num > other._num;
	}
	bool operator==(const object& other)
	{
		return _num == other._num;
	}
	friend std::ostream& operator<<(std::ostream& output, const object& n)
	{
		output << n._num;
		return output;
	}
};
