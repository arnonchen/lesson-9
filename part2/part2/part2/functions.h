#pragma once
#include <iostream>

template <typename T>
T compare(T x, T y)
{
	if (x > y)
	{
		return -1;
	}
	if (x == y)
	{
		return 0;
	}
	return 1;

}
template <class T>
void bubbleSort(T* items, int count)
{
	T t;
	for (int a = 1; a < count; a++)
	{
		for (int b = count - 1; b >= a; b--)
		{
			if (items[b - 1] > items[b])
			{
				t = items[b - 1];
				items[b - 1] = items[b];
				items[b] = t;
			}
		}
	}
}
template <class T>
void printArray(T* items, int count)
{
	T t;
	for (int a = 0; a < count; a++)
	{
		std::cout << items[a] << "\n";
	}
}
