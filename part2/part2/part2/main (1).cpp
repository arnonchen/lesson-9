#include "functions.h"
#include <iostream>
#include "myObj.h"

int main() {
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;
	std::cout << "correct print is sorted array" << std::endl;
	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	//char check
	std::cout << "correct print is sorted array" << std::endl;
	const int array_size = 5;
	char charArr[arr_size] = {'a', 'c', 'b', 'e', 'd'};
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < array_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, array_size);
	std::cout << std::endl;
	//class check
	std::cout << "correct print is sorted array" << std::endl;
	const int size = 3;
	object arr[size] = { 3,2,1 };
	bubbleSort<object>(arr, size);
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "correct print is sorted array" << std::endl;
	printArray<object>(arr, size);
	std::cout << std::endl;
	system("pause");
	return 1;
}