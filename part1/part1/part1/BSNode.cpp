#include "BSNode.h"
BSNode::BSNode(std::string data)
{
	_data = data;
	_right = nullptr;
	_left = nullptr;
	_count = 1;
}

BSNode::BSNode(const BSNode& other)
{
	this->_data = other.getData();
	this->_left = other.getLeft();
	this->_right = other.getRight();
	this->_count = other.getCount() + 1;
}
BSNode::~BSNode()
{

}

BSNode& BSNode::operator=(const BSNode& other)
{
	this->_data = other.getData();
	this->_left = other.getLeft();
	this->_right = other.getRight();
	this->_count = other.getCount() + 1;
	return *this;
}


void BSNode::insert(std::string value)
{
	if (_data.compare(value) == 0)
	{
		_count++;
	}
	else if (_data.compare(value) < 0)
	{
		if (_right != nullptr)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
		}
	}
	else if (_data.compare(value) > 0)
	{
	if (_left != nullptr)
	{
		_left->insert(value);
	}
	else
	{
		_left = new BSNode(value);
	}
	}
}

bool BSNode::search(std::string val) const
{
	if (_data == val)
		return true;
	else if (_data.compare(val) < 0)
	{
		if (_right->search(val))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (_data.compare(val) > 0)
	{
		if (_left->search(val))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
		return false;
}


bool BSNode::isLeaf() const
{
	if (_left == nullptr && _right == nullptr)
	{
		return true;
	}
	return false;
}


std::string BSNode::getData() const
{
	return _data;
}
BSNode* BSNode::getLeft() const
{
	return _left;
}
BSNode* BSNode::getRight() const
{
	return _right;
}
int BSNode::getCount() const
{
	return _count;
}

int BSNode::getHeight() const
{
	int rDepth = 0;
	int lDepth = 0;
	if (_left != nullptr)
		lDepth = _left->getHeight();
	if (_right != nullptr)
		rDepth = _right->getHeight();
	if (lDepth > rDepth)
		return (lDepth + 1);
	else
		return (rDepth + 1);
}
int BSNode::getDepth(const BSNode& root) const
{
	int dist = -1;
	if (_data.compare(root.getData()) == 0)
	{
		return dist + 1;
	}
	else if (_data.compare(root.getData()) < 0)
	{
		if (root.getLeft() != nullptr)
		{
			dist = getDepth(*root.getLeft());
		}
		else
		{
			return -1;
		}
		return dist + 1;
	}
	else if (_data.compare(root.getData()) > 0)
	{
		if (root.getRight() != nullptr)
		{
			dist = getDepth(*root.getRight());
		}
		else
		{
			return -1;
		}
		return dist + 1;
	}
	return dist;
}
void BSNode::printNodes() const
{
	if (_left != nullptr)
	{
		_left->printNodes();
	}
	std::cout << _data << "\n";
	if (_right != nullptr)
	{
		_right->printNodes();
	}
	std::cout << _data << "\n";
}