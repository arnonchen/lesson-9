#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#define SIZE 15
using std::cout;
using std::endl;
using std::string;
int main()
{
	string strArr[SIZE] = { "f" ,"a" ,"c" ,"l" ,"o" ,"n" ,"b" ,"d" ,"e" ,"g" ,"h" ,"j" ,"i" ,"m" ,"k" };
	int intArr[SIZE] = { 7, 3, 2, 5, 4, 6, 9, 1, 8, 11, 0, 14, 13, 12, 10};
	cout << "before sort\nstrings\n";
	for (int i = 0; i < SIZE - 1; i++)
	{
		cout << strArr[i] << ", ";
	}
	cout << strArr[SIZE - 1];
	cout << "\nints\n";
	for (int i = 0; i < SIZE - 1; i++)
	{
		cout << intArr[i] << ", ";
	}
	cout << intArr[SIZE - 1];
	
	BSNode<int>* intTree = new BSNode<int>(intArr[0]);
	BSNode<string>* strTree = new BSNode<string>(strArr[0]);
	for (int i = 1; i < SIZE; i++)
	{
		intTree->insert(intArr[i]);
		strTree->insert(strArr[i]);
	}
	cout << "\nints\n";
	intTree->printNodes();
	cout << "\nstrings\n";
	strTree->printNodes();
	return 0;
}