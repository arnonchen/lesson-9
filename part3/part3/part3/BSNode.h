#pragma once
#include <iostream>
#include <string>
template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;
	int getCount() const;
	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

};
template <class T>
int Compare(T x, T y)
{
	if (x > y)
	{
		return -1;
	}
	if (x == y)
	{
		return 0;
	}
	return 1;
}
template <class T>
BSNode<T>::BSNode(T data)
{
	_data = data;
	_right = nullptr;
	_left = nullptr;
	_count = 1;
}
template <class T>
BSNode<T>::BSNode(const BSNode& other)
{
	this->_data = other.getData();
	this->_left = other.getLeft();
	this->_right = other.getRight();
	this->_count = other.getCount() + 1;
}
template <class T>
BSNode<T>::~BSNode()
{

}
template <class T>
BSNode<T>&  BSNode<T>::operator=(const BSNode& other)
{
	this->_data = other.getData();
	this->_left = other.getLeft();
	this->_right = other.getRight();
	this->_count = other.getCount() + 1;
	return *this;
}

template <class T>
void BSNode<T>::insert(T value)
{
	if (Compare<T>(value, _data) == 0)
	{
		_count++;
	}
	else if (Compare<T>(_data, value) > 0)
	{
		if (_right != nullptr)
		{
			_right->insert(value);
		}
		else
		{
			_right = new BSNode(value);
		}
	}
	else if (Compare<T>(_data, value) < 0)
	{
		if (_left != nullptr)
		{
			_left->insert(value);
		}
		else
		{
			_left = new BSNode(value);
		}
	}
}
template <class T>
bool BSNode<T>::search(T val) const
{
	if (_data == val)
		return true;
	else if (_data.compare(val) < 0)
	{
		if (_right->search(val))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (_data.compare(val) > 0)
	{
		if (_left->search(val))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
		return false;
}
template <class T>
bool BSNode<T>::isLeaf() const
{
	if (_left == nullptr && _right == nullptr)
	{
		return true;
	}
	return false;
}
template <class T>
T BSNode<T>::getData() const
{
	return _data;
}
template <class T>
BSNode<T>*  BSNode<T>::getLeft() const
{
	return _left;
}
template <class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return _right;
}
template <class T>
int BSNode<T>::getCount() const
{
	return _count;
}

template <class T>
int BSNode<T>::getHeight() const
{
	int rDepth = 0;
	int lDepth = 0;
	if (_left != nullptr)
		lDepth = _left->getHeight();
	if (_right != nullptr)
		rDepth = _right->getHeight();
	if (lDepth > rDepth)
		return (lDepth + 1);
	else
		return (rDepth + 1);
}

template <class T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	int dist = -1;
	if (_data == root.getData())
	{
		return dist + 1;
	}
	else if (_data < root.getData())
	{
		if (root.getLeft() != nullptr)
		{
			dist = getDepth(*root.getLeft());
		}
		else
		{
			return -1;
		}
		return dist + 1;
	}
	else if (_data > root.getData())
	{
		if (root.getRight() != nullptr)
		{
			dist = getDepth(*root.getRight());
		}
		else
		{
			return -1;
		}
		return dist + 1;
	}
	return dist;
}

template <class T>
void BSNode<T>::printNodes() const
{
	if (!this)
	{
		return;
	}
	_left->printNodes();
	std::cout << _data << ", ";
	_right->printNodes();
}